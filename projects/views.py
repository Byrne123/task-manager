from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_list(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project,
    }
    return render(request, "list.html", context)


@login_required
def show_project_details(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "create.html", context)
